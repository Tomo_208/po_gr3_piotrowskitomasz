package pl.imiajd.piotrowski;

public class Student extends Osoba{

    private String kierunek;


    public Student(String nazwisko, int rokUrodzenia, String kierunek)
    {
        super(nazwisko, rokUrodzenia);
        this.kierunek = kierunek;
    }


    public String getKierunek()
    {
        return this.kierunek;
    }

    @Override
    public String toString() {
        return String.format(super.toString() + " " + this.kierunek);
    }
}
