package pl.imiajd.piotrowski;
import java.awt.Rectangle;

public class BetterRetangle {

    private int x;
    private int y;

    public BetterRetangle(int x, int y)
    {
        this.x = x;
        this.y = y;

        Rectangle r = new Rectangle();
        r.setSize(x, y);
        r.setLocation(x, y);
    }

    public int getPerimeter()
    {
        return  2 * this.x + 2 * this.y;
    }

    public int GetArea()
    {
        return this.x * this.y;
    }
}