package pl.imiajd.piotrowski;

public class Nauczyciel extends Osoba{

    double pensja;

    public Nauczyciel(String nazwisko, int rokUrodzenia, double pensja)
    {
        super(nazwisko, rokUrodzenia);
        this.pensja = pensja;
    }



    public double getPensja()
    {
        return this.pensja;
    }

    @Override
    public String toString() {
        return String.format(super.toString() + " " + this.pensja);
    }
}
