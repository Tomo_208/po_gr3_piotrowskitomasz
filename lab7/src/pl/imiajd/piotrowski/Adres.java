package pl.imiajd.piotrowski;

public class Adres {
    private String ulica;
    private int numer_domu;
    private int numer_mieszkania;
    public String kod_pocztowy;
    private String miasto;


    public Adres(String ulica, int numer_domu, int numer_mieszkania, String kod_pocztowy, String miasto)
    {
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.numer_mieszkania = numer_mieszkania;
        this.kod_pocztowy = kod_pocztowy;
        this.miasto = miasto;

    }

    public Adres(String ulica, int numer_domu,  String kod_pocztowy, String miasto)
    {
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.kod_pocztowy = kod_pocztowy;
        this.miasto = miasto;

    }

    public void pokaz()
    {
        System.out.println(this.kod_pocztowy + "," + this.miasto);
        System.out.println(this.ulica + "," + this.numer_domu);
    }

    public boolean przed(String kod_pocztowy2)
    {
//
//           int num2 = Integer.parseInt(kod_pocztowy2);
//           int num = Integer.parseInt(this.kod_pocztowy);

        return Integer.parseInt(this.kod_pocztowy) < Integer.parseInt(kod_pocztowy2);

    }
}
