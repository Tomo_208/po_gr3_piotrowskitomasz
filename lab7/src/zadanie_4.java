import pl.imiajd.piotrowski.*;

public class zadanie_4 {

    public static void main(String[] args) {

        Osoba o = new Osoba("Waszka", 1987);
        Student s = new Student("Iksa", 2912, "Wojak");
        Nauczyciel n = new Nauczyciel("Miles", 1999, 3912);

        System.out.println(o);
        System.out.println(s.getKierunek());
        System.out.println(n);

        System.out.println(n.getNazwisko());
    }
}