import java.util.Locale;
import java.util.Stack;



public class zad5 {
    public static void main(String[] args) {

        reverse("Ala ma kota. Jej kot lubi myszy.");
    }

    public static  void reverse(String text) {
        Stack<String> p = new Stack<>();

        String[] words = text.split(" ");
        boolean first = true;

        for (String ch : words) {
            if (first) {
//                System.out.println(ch.toLowerCase(Locale.ROOT) + ".");
                p.push(ch.toLowerCase(Locale.ROOT) + ".");
                first = false;
            } else if (ch.charAt(ch.length() - 1) == '.') {
//                System.out.println(ch.substring(0,1).toUpperCase() + ch.substring(1, ch.length() -1).toLowerCase());
                p.push(ch.substring(0, 1).toUpperCase() + ch.substring(1, ch.length() - 1).toLowerCase());
                printStack(p);
                first = true;
            } else {
//                System.out.println(ch);
                p.push(ch);
            }

        }
    }

    public static void printStack(Stack<String> s)
    {
        if (s.empty())
            return;

        System.out.print(s.peek() + " ");
        s.pop();
        printStack(s);
    }
}