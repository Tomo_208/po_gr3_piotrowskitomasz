import java.util.Stack;

public class zad6 {

    public static void main(String[] args)
    {
        divider(2015);
    }

    public static void divider(int number)
    {
        Stack<Integer> p = new Stack<>();

        while(number > 0)
        {
            p.push(number % 10);

            number = number / 10;
        }

        printStack(p);
    }

    public static void printStack(Stack<Integer> s)
    {
        if (s.empty())
            return;

        System.out.print(s.peek() + " ");
        s.pop();
        printStack(s);
    }
}