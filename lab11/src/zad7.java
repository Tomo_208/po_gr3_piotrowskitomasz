import java.util.HashSet;
import java.util.Set;

public class zad7 {
    public static void main(String[] args) {

        greekNumbers(21);
    }

    public static void greekNumbers(int n)
    {
        int endNum = (int) Math.sqrt(n) + 1;
        Set<Integer> s = new HashSet<>();
        Set<Integer> newS = new HashSet<>();
        fillSet(s, n);
        fillGreekMethodSet(newS, endNum, n);
        s.removeAll(newS);
        display(s);
        // zad8.print(s);

    }

    public static void fillSet(Set<Integer> s, int n)
    {
        for(int i=2; i<=n; i++)
        {
            s.add(i);
        }
    }

    public static void fillGreekMethodSet(Set<Integer> newS, int endNum, int n)
    {
        for(int i = 2; i <= n; i++) {
            for (int j = i; j <= n; j+=i) {
                if(i >= endNum)
                    return;
                if(j > i)
                    newS.add(j);
            }
        }
    }

    public static void display(Set<Integer> primes)
    {
        for(Integer x: primes)
        {
            System.out.print(x + " ");
        }
    }
}