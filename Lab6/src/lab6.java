
public class lab6 {
    public static void main(String[] args) {

        Rachunek_bankowy sever1 = new Rachunek_bankowy();
        Rachunek_bankowy sever2 = new Rachunek_bankowy();
        sever1.saldo = 2000;
        sever2.saldo = 3000;
        sever1.roczna_stopa_procentowa = 0.04;
        sever2.roczna_stopa_procentowa = 0.04;
        System.out.println(sever1.oblicz_miesieczne_odsetki());
        System.out.println(sever2.oblicz_miesieczne_odsetki());
        sever1.set_roczna_stopa_procentowa(0.05);
        sever2.set_roczna_stopa_procentowa(0.05);
        System.out.println(sever1.oblicz_miesieczne_odsetki());
        System.out.println(sever2.oblicz_miesieczne_odsetki());
    }




    public static class Rachunek_bankowy{

        double roczna_stopa_procentowa;
        private double saldo;

        public double oblicz_miesieczne_odsetki()
        {
            double odestki = (roczna_stopa_procentowa * saldo)/12;
            saldo = saldo + odestki;

            return saldo;


        }

        public void set_roczna_stopa_procentowa(double nowa)
        {
            roczna_stopa_procentowa = nowa;
        }


    }

}