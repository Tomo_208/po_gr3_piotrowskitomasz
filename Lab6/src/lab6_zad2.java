import java.util.ArrayList;

public class lab6_zad2
{
    public static void main(String[] args)
    {
        lab6_zad2 test = new lab6_zad2();

        ArrayList<Integer> list = new ArrayList<>();
        ArrayList<Integer> newL = new ArrayList<>();

        for(int i=0; i<100; i++)
        {
            list.add(i);
            newL.add(i + 2);
        }

        test.tmp = list;

        //System.out.println(test.union(newL));
        //System.out.println(test.intersection(newL));
        System.out.println(test.tmp);
        test.insertElement(3);
        System.out.println(test.tmp);
        test.deleteElement(5);
        test.toString();
        test.equals(newL);
    }
    public ArrayList<Integer> tmp;

    public ArrayList<Integer> union(ArrayList<Integer> newList)
    {
        tmp.addAll(newList);
        return tmp;
    }

    public ArrayList<Integer> intersection(ArrayList<Integer> newList)
    {
        tmp.retainAll(newList);
        return tmp;
    }

    void insertElement (int value)
    {
        for(int i=0; i<tmp.size(); i++)
        {
            if(tmp.get(i) == value) {
                tmp.add(i);
                break;
            }
        }
    }

    public String toString()
    {
        String listString = tmp.toString();

        return listString;
    }

    public void deleteElement(int value)
    {
        for(int i=0; i<tmp.size(); i++)
        {
            if(tmp.get(i) == value)
            {
                tmp.remove(value);
            }
        }
    }

    public Boolean equals(ArrayList<Integer> newList)
    {
        Boolean bool = tmp.equals(newList);
        return bool;
    }


}