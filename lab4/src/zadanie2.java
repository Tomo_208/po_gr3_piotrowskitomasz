import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class zadanie2 {
    public static void main(String[] args)throws FileNotFoundException {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj nazwe pliku: ");
        String nazwa_pliku = scan.next();
        System.out.println("Podaj znak: ");
        char znak = scan.next().charAt(0);
        System.out.println(countChar(nazwa_pliku,znak));


    }
    public static int countChar(String plik, char c)throws FileNotFoundException
    {
        int licznik = 0;
        File file = new File(plik);
       Scanner in = new Scanner(file);
        while (in.hasNext()) {
            String zdanie = in.nextLine();
            for (int i = 0; i < zdanie.length(); i++) {
                if (c == zdanie.charAt(i)) {
                    licznik = licznik + 1;
                }
            }
        }
        return licznik;
    }
}
