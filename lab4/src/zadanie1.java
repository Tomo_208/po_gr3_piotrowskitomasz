import java.util.Arrays;
import java.util.Scanner;
import java.lang.String;

public class zadanie1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj napis: ");
        String napis = scan.next();


        System.out.println("Podaj znak: ");
        char znak = scan.next().charAt(0);

        System.out.println("Podaj drugi napis: ");
        String napis2 = scan.next();


        System.out.println(countChar(napis, znak));
        System.out.println(countSubStr(napis,napis2));
        System.out.println(middle(napis));
        System.out.println(repeat(napis,3));
        System.out.println(Arrays.toString(where(napis, napis2)));
        System.out.println(change("asdfDDDD"));
        System.out.println(nice("1234567890"));
        System.out.println(nice_modify("1234567890", 2, znak));


    }
    public static int countChar(String str, char c)
    {   int licznik = 0;
        for(int i =0; i < str.length(); i++)
        {
            if(c == str.charAt(i))
            {
                licznik = licznik +1;
            }
        }
        return licznik;
    }

    public static int countSubStr(String str, String subStr )
    {   int licznik = 0;
        int index = 0;
        int pierwszy_index = str.indexOf(subStr, index);
//        int ostatni_index = str.lastIndexOf(subStr);

        while(pierwszy_index != -1)
        {
            pierwszy_index = str.indexOf(subStr, index);
            if(pierwszy_index != -1)
            {
                index = pierwszy_index +1;
                licznik = licznik +1;
            }


        }
        return licznik;
    }

    public static String middle(String str)
    {   int index =0;
        if(str.length() % 2 == 1)
        {
            index = str.length() / 2;

            return str.substring(index, index +1);
        }

        if(str.length() % 2 == 0)
        {
            index = str.length() / 2;
        }

        return str.substring(index -1 ,index +1);


    }

    public static String repeat(String str, int n)
    {   String napis = "";
        for(int i =0; i < n; i++)
        {
            napis = napis + str;
        }
        return napis;
    }

    public static int[] where(String str, String subStr )
    {   int licznik = 0;
        int[] tab = new  int[str.length()];
        int index = 0;
        int pierwszy_index = str.indexOf(subStr, index);
        while(pierwszy_index != -1)
        {
            pierwszy_index = str.indexOf(subStr, index);
            if(pierwszy_index != -1)
            {
                tab[licznik] = pierwszy_index;
                licznik = licznik + 1;
                index = pierwszy_index +1;
            }
        }
        return tab;
    }


    public static String change(String str)
    {
        StringBuffer napis = new StringBuffer();
        napis.append(str);
        String napis2 = napis.toString();
        String napis_koncowy = "";
        char znak;

        for(int i =0; i < napis2.length(); i++)
        {

            if(Character.isLowerCase(napis2.charAt(i)))
            {
                 znak = Character.toUpperCase(napis2.charAt(i));
            }
            else
            {
                 znak = napis2.toLowerCase().charAt(i);
            }
            napis_koncowy = napis_koncowy + znak;
        }

        return napis_koncowy;

    }

    public static String nice(String str)
    {   int licznik = 0;
        StringBuffer napis = new StringBuffer();
        napis.append(str);
        String znak = ":";

        for(int i = napis.length()-1; i > 0; i--)
        {
            licznik = licznik +1;
            if(licznik ==3)
            {
                licznik = 0;
                napis.insert(i,znak.charAt(0));

            }
        }
        return napis.toString();
    }

    public static String nice_modify(String str, int index, char znak)
    {
        int licznik = 0;
        StringBuffer napis = new StringBuffer();
        napis.append(str);


        for(int i = napis.length()-1; i > 0; i--)
        {
            licznik = licznik +1;
            if(licznik == index)
            {
                licznik = 0;
                napis.insert(i,znak);

            }
        }
        return napis.toString();


    }




    }