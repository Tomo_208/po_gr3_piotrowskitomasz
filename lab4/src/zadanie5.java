import java.math.BigDecimal;
import java.math.BigInteger;

public class zadanie5 {
    public static void main(String[] args) {
        BigDecimal k = new BigDecimal(args[0]);
        BigDecimal p = new BigDecimal(args[1]);
        BigDecimal n = new BigDecimal(args[2]);

        BigDecimal wynik = k;

        for (BigInteger i = BigInteger.valueOf(0); i.compareTo(n.toBigInteger()) < 0; i = i.add(BigInteger.ONE)){
            wynik = wynik.add(wynik.multiply(p));
        }
        System.out.print(wynik);

    }
}
