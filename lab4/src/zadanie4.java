import java.math.BigInteger;

public class zadanie4 {
    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        BigInteger pole = BigInteger.valueOf(1);
        BigInteger suma = BigInteger.valueOf(0);

            for (int i = 0; i<n*n; i++){
                suma = suma.add(pole);
                pole = pole.multiply(BigInteger.valueOf(2));
            }

        System.out.println(suma);


    }
}
