package pl.imiajd.piotrowski;

import java.time.LocalDate;

public class Osoba{
    private String nazwisko;
    private int rokUrodzenia;
    private String imie;
    private boolean plec;
    private LocalDate data_urodzenia;

    public Osoba(String nazwisko, int rokUrodzenia,String imie, boolean plec, LocalDate data_urodzenia)
    {
        this.nazwisko = nazwisko;
        this.rokUrodzenia = rokUrodzenia;
        this.imie = imie;
        this.plec = plec;
        this.data_urodzenia = data_urodzenia;
    }

    public String getNazwisko()
    {
        return this.nazwisko;
    }

    public int getRokUrodzenia()
    {
        return this.rokUrodzenia;
    }

    public String getImie(){return this.imie;}

    public boolean getPlec(){return this.plec;}

    public LocalDate getData_urodzenia(){return  this.data_urodzenia;}




    @Override
    public String toString() {
        return String.format(this.nazwisko + " " + this.rokUrodzenia);
    }
}