package pl.imiajd.piotrowski;

import pl.imiajd.piotrowski.Osoba;

import java.time.LocalDate;

public class Student extends Osoba {

    private String kierunek;
    private double srednia_Ocen;


    public Student(String nazwisko, int rokUrodzenia, String imie, boolean plec, LocalDate data_urodzenia,double srednia_Ocen,String kierunek)
    {
        super(nazwisko, rokUrodzenia, imie, plec, data_urodzenia);
        this.kierunek = kierunek;
        this.srednia_Ocen = srednia_Ocen;
    }


    public String getKierunek()
    {
        return this.kierunek;
    }

    public double getSrednia_Ocen() {
        return this.srednia_Ocen;
    }

    public void setSrednia_Ocen(double new_Srednia){this.srednia_Ocen = new_Srednia;}

    @Override
    public String toString() {
        return String.format(super.toString() + " " + this.kierunek);
    }
}