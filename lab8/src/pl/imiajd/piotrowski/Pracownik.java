package pl.imiajd.piotrowski;
import pl.imiajd.piotrowski.Osoba;

import java.time.LocalDate;

public class Pracownik extends Osoba {

    double pensja;
    LocalDate data_zatrudnienia;

    public Pracownik(String nazwisko, int rokUrodzenia, String imie, boolean plec, LocalDate data_urodzenia, double pensja, LocalDate data_zatrudnienia)
    {
        super(nazwisko, rokUrodzenia, imie, plec, data_urodzenia);
        this.pensja = pensja;
        this.data_zatrudnienia = data_zatrudnienia;
    }



    public double getPensja()
    {
        return this.pensja;
    }

    public LocalDate getData_zatrudnienia(){return this.data_zatrudnienia;}

    @Override
    public String toString() {
        return String.format(super.toString() + " " + this.pensja);
    }
}