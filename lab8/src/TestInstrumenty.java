import java.time.LocalDate;
import pl.imiajd.piotrowski.*;

public class TestInstrumenty {
    public static void main(String[] args) {
        Instrument[] orkiestra = new Instrument[5];

        LocalDate l = LocalDate.parse("2000-10-19");

        orkiestra[0] = new Fortepian("po", l);
        orkiestra[1] = new Flet("xd", l);
        orkiestra[2] = new Skrzypce("wow", l);
        orkiestra[3] = new Flet("vevo", l);
        orkiestra[4] = new Fortepian("Chopin", l);

        for(Instrument p : orkiestra)
        {
            p.dzwiek();
        }

        for(int i =0; i<orkiestra.length; i++ )
        {

            System.out.println(orkiestra[i]);

        }

    }
}