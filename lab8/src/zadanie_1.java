import java.time.LocalDate;
import pl.imiajd.piotrowski.Osoba;
import pl.imiajd.piotrowski.Pracownik;
import pl.imiajd.piotrowski.Student;

class zadanie_1 {

    public static void main(String[] args) {

        Osoba o = new Osoba("Waszka", 1987, "imie", true, LocalDate.of(2000, 1, 8));
        Student s = new Student("Waszka", 1987, "imie", true, LocalDate.of(2000, 1, 8), 3, "informatyka");
        Pracownik n = new Pracownik("Waszka", 1987, "imie", true, LocalDate.of(2000, 1, 8), 3656, LocalDate.of(2020, 1, 8));

        System.out.println(o);
        System.out.println(s.getKierunek());
        System.out.println(s.getSrednia_Ocen());
        s.setSrednia_Ocen(4.0);
        System.out.println(s.getSrednia_Ocen());


        System.out.println(n);

        System.out.println(n.getNazwisko());
    }
}