package pl.imiajd.piotrowski;

import java.time.LocalDate;
import java.util.Objects;

public  class  Osoba implements Cloneable,Comparable {
    private String nazwisko;
    private LocalDate dataUrodzenia;

    public  Osoba(String nazwisko, LocalDate dataUrodzenia) {
        this.dataUrodzenia = dataUrodzenia;
        this.nazwisko = nazwisko;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "[" + this.nazwisko + ", " + this.dataUrodzenia+ "]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Osoba osoba = (Osoba) o;
        return Objects.equals(nazwisko, osoba.nazwisko) && Objects.equals(dataUrodzenia, osoba.dataUrodzenia);
    }

    @Override
    public  int compareTo(Object o) {
        if (this == o) return 0;
        if (o == null || getClass() != o.getClass()) return 1;
        Osoba osoba = (Osoba) o;
        if(nazwisko.compareTo(osoba.nazwisko) == 0)
        {
            if(dataUrodzenia.compareTo(osoba.dataUrodzenia) == 0)
            {
                return 0;
            }

        }
        return 1;
    }
}
