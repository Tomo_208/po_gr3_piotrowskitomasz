import java.util.Random;



public class zadanie2_f {

    public static void main(String[] args) {
        int[] tab = new int[10];
        generuj(tab, 10, -999, 1999);


        signum(tab);


    }

    public static void generuj(int[] tab, int n, int minWartosc, int maxWartosc) {


        Random generator = new Random();

        for (int i = 0; i < n; i++) {
            tab[i] = generator.nextInt(maxWartosc) + minWartosc;
        }
        System.out.println(java.util.Arrays.toString(tab));

    }



    public static void signum(int[] tab)
    {
        for(int i = 0; i < tab.length; i++ )
        {
            if(tab[i] > 0 )
            {
                tab[i] = 1 ;
            }

            if(tab[i] < 0)
            {
                tab[i] = -1;
            }
        }
        System.out.println(java.util.Arrays.toString(tab));
    }
}
