import java.util.Random;


public class zadanie2_a {

    public static void main(String[] args) {
        int[] tab = new int[15];
        generuj(tab, 15, -999, 1999);

        System.out.println(IleParzystych(tab));
        System.out.println(IleNieParzystych(tab));


    }

    public static void generuj(int[] tab, int n, int minWartosc, int maxWartosc) {


        Random generator = new Random();

        for (int i = 0; i < n; i++) {
            tab[i] = generator.nextInt(maxWartosc) + minWartosc;
        }
        System.out.println(java.util.Arrays.toString(tab));

    }


    public static int IleParzystych(int[] tab) {

        int licznik_parzystych = 0;

        for (int i = 0; i < tab.length; i++) {
            if (tab[i] % 2 == 0) {
                licznik_parzystych = licznik_parzystych + 1;
            }

        }
        return licznik_parzystych;

    }
    public static int IleNieParzystych(int[] tab) {

        int licznik_nieparzystych = 0;

        for (int i = 0; i < tab.length; i++) {
            if (tab[i] % 2 == 1 || tab[i] % 2 == -1) {
                licznik_nieparzystych = licznik_nieparzystych + 1;
            }

        }
        return licznik_nieparzystych;

    }
}
