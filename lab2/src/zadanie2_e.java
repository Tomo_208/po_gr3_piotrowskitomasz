import java.util.Random;
import java.util.ArrayList;
import java.util.List;


public class zadanie2_e {

    public static void main(String[] args) {
        int[] tab = new int[10];
        generuj(tab, 10, -999, 1999);


        System.out.println(dlugoscMaksymalnegoCiaguDodatnich(tab));


    }

    public static void generuj(int[] tab, int n, int minWartosc, int maxWartosc) {


        Random generator = new Random();

        for (int i = 0; i < n; i++) {
            tab[i] = generator.nextInt(maxWartosc) + minWartosc;
        }
        System.out.println(java.util.Arrays.toString(tab));

    }



    public static int dlugoscMaksymalnegoCiaguDodatnich(int[] tab)
    {   List<Integer> lista1 = new ArrayList<>();
        int licznik_dlugosci = 0;
        for(int i = 0; i < tab.length; i++)
        {
            if(tab[i] > 0)
            {
                licznik_dlugosci = licznik_dlugosci + 1;
            }
            else
            {
                lista1.add(licznik_dlugosci);
                licznik_dlugosci = 0;
            }
        }
        lista1.add(licznik_dlugosci); //jesli ciag konczy sie na elementach dodatnich to dodaje ten ciag do listy
        int zm = lista1.get(0);


        for (int i = 1; i < lista1.size(); i++) {

            if(lista1.get(i) > zm)
            {
                zm = lista1.get(i);
            }

        }
        return zm;
    }
}
