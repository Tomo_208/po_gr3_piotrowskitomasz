import java.util.Random;


public class zadanie2_d {

    public static void main(String[] args) {
        int[] tab = new int[5];
        generuj(tab, 5, -999, 1999);


        System.out.println(sumaDodatnich(tab));
        System.out.println(sumaUjemnych(tab));


    }

    public static void generuj(int[] tab, int n, int minWartosc, int maxWartosc) {


        Random generator = new Random();

        for (int i = 0; i < n; i++) {
            tab[i] = generator.nextInt(maxWartosc) + minWartosc;
        }
        System.out.println(java.util.Arrays.toString(tab));

    }



    public static int sumaDodatnich(int[] tab)
    {       int suma_dodatnich = 0;
        for (int i = 0; i < tab.length; i++) {
            if(tab[i] > 0)
            {
                suma_dodatnich = suma_dodatnich + tab[i];
            }
        }
        return suma_dodatnich;
    }
    public static int sumaUjemnych(int[] tab)
    {       int suma_ujemnych = 0;
        for (int i = 0; i < tab.length; i++) {
            if(tab[i] < 0)
            {
                suma_ujemnych = suma_ujemnych + tab[i];
            }
        }
        return suma_ujemnych;
    }
}
