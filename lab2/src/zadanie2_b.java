import java.util.Random;


public class zadanie2_b {

    public static void main(String[] args) {
        int[] tab = new int[100];
        generuj(tab, 100, -999, 1999);

        System.out.println(IleDodatnich(tab));
        System.out.println(IleUjemnych(tab));
        System.out.println(IleZerowych(tab));


    }

    public static void generuj(int[] tab, int n, int minWartosc, int maxWartosc) {


        Random generator = new Random();

        for (int i = 0; i < n; i++) {
            tab[i] = generator.nextInt(maxWartosc) + minWartosc;
        }
        System.out.println(java.util.Arrays.toString(tab));

    }


  public static int IleDodatnich(int[] tab)
  {     int licznik_dodatnich = 0;
      for (int i = 0; i < tab.length; i++) {
          if (tab[i] > 0) {
              licznik_dodatnich = licznik_dodatnich + 1;
          }

      }
      return licznik_dodatnich;
  }

    public static int IleUjemnych(int[] tab)
    {     int licznik_ujemnych = 0;
        for (int i = 0; i < tab.length; i++) {
            if (tab[i] < 0) {
                licznik_ujemnych = licznik_ujemnych + 1;
            }

        }
        return licznik_ujemnych;
    }
    public static int IleZerowych(int[] tab)
    {     int licznik_zerowych = 0;
        for (int i = 0; i < tab.length; i++) {
            if (tab[i] == 0) {
                licznik_zerowych = licznik_zerowych + 1;
            }

        }
        return licznik_zerowych;
    }
}
